let crypto = require('crypto')
let async = require('async')

let mongoose = require('../lib/mongoose'),
    Schema = mongoose.Schema

let schema = new Schema({
    email: {
        type: String,
        unique: true,
        require: true
    },
    image:{
        type:String
    },
    username: {
        type: String,
    },
    hashedPassword: {
        type: String,
    },
    salt: {
        type: String,
    },
    created: {
        type: Date,
        default: Date.now
    },


})

schema.methods.encryptPassword = function (password) {
    return crypto.createHmac('sha1', this.salt).update(password).digest('hex')
}

schema.virtual('password')
    .set(function (password) {
        this._planPassword = password
        this.salt = Math.random() + ''
        this.hashedPassword = this.encryptPassword(password)
    })
    .get(function () {
        return this._planPassword
    })

schema.methods.checkPassword = function (password) {
    return this.encryptPassword(password) === this.hashedPassword
}


exports.UserModel = mongoose.model('user', schema)
