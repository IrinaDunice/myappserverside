let mongoose = require('mongoose'),
    config = require('../config')

// mongoose.connect('mongodb://localhost/blog')

mongoose.connect(config.get('mongoose:uri'))

mongoose.createConnection(config.get('mongoose:uri'))


module.exports = mongoose

