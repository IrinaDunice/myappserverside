const express = require('express'),
  router = express.Router(),
  User = require('../models/user').UserModel;
let log = require('../lib/log')(module);

router.post('/', function (req, res, next) {
  console.log('req--', req.body);

  let user = new User({
    username: req.body.user.username,
    firstname: req.body.user.firstname,
    lastname: req.body.user.lastname,
    email: req.body.user.email,
    password: req.body.user.password
  });

  user.save(function (err, user) {
    if (!err) {
      log.info("New user - %s:%s", user.username, user.password);
      console.log('user res', user)
      res.send(user)
    } else {
      return log.error(err);
    }
  });

});


router.get('/', function (req, res, next) {

  res.send('respond with a resource');
});

module.exports = router;
