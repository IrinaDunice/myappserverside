const express = require('express'),
  router = express.Router(),
  passport = require('../lib/oauth'),
  Users = require('../models/user').UserModel;
let log = require('../lib/log')(module);
const jwtsecret = "mysecretkey";
const jwt = require('jsonwebtoken');

// router.get('/',function (req, res, next)  {
//   console.log('req', req)
//   res.status(200).json({ });
// })
router.get('/', passport.authenticate('jwt',{
  session: false
}), serialize)

function serialize(req, res, next) {
  Users.find(function (err, users) {
    if(!err){
      res.status(200).json({
        users: users
      });
    }else {
      res.send(err)
    }
  })
}
module.exports = router;