const express = require('express'),
  router = express.Router(),
  passport = require('../lib/oauth'),
  Users = require('../models/user').UserModel;
const jwt = require('jsonwebtoken');


router.get('/', passport.authenticate('jwt',{
  session: false
}), getProfile)

function getProfile(req, res, next) {
  Users.findById({_id:req.user._id},function (err, user) {
    if(!err){
      res.status(200).json({
        user: user
      });
    }else {
      res.send(err)
    }
  })
}
module.exports = router;