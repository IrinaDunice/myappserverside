const express = require('express'),
  router = express.Router(),
  passport = require('../lib/oauth'),
  Users = require('../models/user').UserModel;

const jwtsecret = "mysecretkey";
const jwt = require('jsonwebtoken');


router.post('/', passport.authenticate(
  'local', {
    session: false
  }), serialize, generateToken, respond);

router.get('/', function (req, res, next) {
  res.send('respond with a resource');
})


function serialize(req, res, next) {
  Users.findById(req.user._id, function(err, user){
    if(err) {return next(err);}
    req.user = {
      id: user._id
    };
    next();
  });
}
function generateToken(req, res, next) {
 req.token = jwt.sign({
    id: req.user.id,
  }, jwtsecret);
  next();
}
function respond(req, res) {
  res.status(200).json({
    user: req.user,
    token: req.token
  });
}
module.exports = router;

