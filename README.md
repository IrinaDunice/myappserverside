# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Объекты в БД ###
* User — пользователь, который обладает именем, хэшем пароля и солью своего пароля.
* Client — клиент-приложение, которому выдается доступ от имени пользователя. Обладают именем и секретным кодом.
* AccessToken — токен (тип bearer), выдаваемый клиентским приложениям, ограничен по времени.
* RefreshToken — другой тип токена, позволяет запросить новый bearer-токен без повторного запроса пароля у пользователя.